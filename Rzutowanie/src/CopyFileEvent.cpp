/*
 * CopyFileEvent.cpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#include "CopyFileEvent.h"
const std::string CopyFileEvent::eventType = "CopyFileEvent";

CopyFileEvent::CopyFileEvent(std::string nameCopyFile):copyFileName(nameCopyFile) {

}

CopyFileEvent::~CopyFileEvent() {
	// TODO Auto-generated destructor stub
}
std::string CopyFileEvent::getEventType() const {

	return eventType;
}
std::string CopyFileEvent::getCopyFileName() const {

	return copyFileName;
}

