//============================================================================
// Name        : Rzutowanie.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Event.h"
#include "OpenFileEvent.h"
#include "CopyFileEvent.h"

using namespace std;

void displayEvent(Event *event)
{
	cout << "Recived: " << event->getEventType() << " ";
	if(event->getEventType() == "OpenFileEvent")
	{
		OpenFileEvent *openEvent = static_cast<OpenFileEvent*>(event);
		cout << "Trying to open file: " << openEvent->getFileName();
	}
	OpenFileEvent *openEvent = dynamic_cast<OpenFileEvent*>(event);
	if(openEvent) {
		cout << " Got an openEvent!";
	}
	if(event->getEventType() == "CopyFileEvent")
	{
		CopyFileEvent *copyEvent = static_cast<CopyFileEvent*>(event);
		cout << "Trying to get to file: " << copyEvent->getCopyFileName();
	}
	cout << "\n";
}

int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
//	float aNumber = 3.5;
//	int someOtherNumber = static_cast<int>(aNumber);
//	int someOtherNumber1 = (int)aNumber;
//	int someOtherNumber2 = int(aNumber);

	Event genEv;
	OpenFileEvent openFEv("a.txt");
	CopyFileEvent copyF ("copy.txt");

//	Event *ev = &genEv;
//	cout << ev->getEventType() << "\n";
//	ev = &openFEv;
//	cout << ev->getEventType() << "\n";

	Event *ev = &genEv;
	displayEvent(ev);
	ev = &openFEv;
	displayEvent(ev);
	ev = &copyF;
	displayEvent(ev);

	return 0;
}
