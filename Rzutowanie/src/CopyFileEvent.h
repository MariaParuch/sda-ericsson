/*
 * CopyFileEvent.h
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef COPYFILEEVENT_H_
#define COPYFILEEVENT_H_

#include "Event.h"

class CopyFileEvent: public Event {
public:
	CopyFileEvent(std::string nameCopyFile);
	virtual ~CopyFileEvent();
	std::string getEventType() const;
	std::string getCopyFileName() const;
private:
	static const std::string eventType;
	std::string copyFileName;
};

#endif /* COPYFILEEVENT_H_ */
