//============================================================================
// Name        : VektorCwiczenia(tablice).cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>

int main() {

	std::vector<int> myvector;
	int element;
	std::cout << "Give five integers" << std::endl;

	for(int i=0; i<5; ++i){
		std::cin >> element;
		myvector.push_back(element);
	}
	std::cout << "Print all elements of vector" << std::endl;
	for(int i=0; i<5; ++i){
		std::cout << myvector[i] << " ";
	}
	std::cout << std::endl;
	std::cout << "Print back all elements of vector" << std::endl;
	for(int i = 4; i>=0; --i) {
		std::cout << myvector[i] << " ";
	}
	std::cout << std::endl;
	std::cout << "Print min & max from vector" << std::endl;
	int max = myvector[1];
	int min = myvector[1];
	for(int i=0; i<myvector.size(); ++i){
		if (myvector[i]>max) {
			max = myvector[i];
		} else if (myvector[i]<min) {
		min = myvector[i];
		}
	}
	std::cout << "Max = " << max << " " << "Min = " << min << std::endl;

	int sum = 0;
	for(int i=0; i<myvector.size(); ++i){
			int temp = myvector[i];
			sum += temp;
	}
	std::cout << "Sum of all elements: " << sum << std::endl;

	int difference = 0;
	for(int i=1; i<myvector.size(); i++){
		difference = (myvector[i]) - (myvector[i-1]);
		std::cout << "difference between previous and next elements " << difference << std::endl;

	}
	std::vector<int> swapvector(5,100);
	myvector.swap(swapvector);
	std::cout << "Print two vectors after swap" << "; "
			<< "myvector:" << std::endl;
	for(int i=0; i<5; ++i){
		std::cout << myvector[i] << " ";
	}
	std::cout << "swapvector:" << std::endl;
	for(int i=0; i<5; ++i){
		std::cout << swapvector[i] << " ";
	}

	int position;
	int number;
	std::cout << "give me a number of position and some integer" << std::endl;
	std::cin >> position;
	std::cin >> number;
	myvector.insert(myvector.begin()+position, number);
	std::cout << "current vector size: " << myvector.size() << std::endl;
	std::cin >> position;
	myvector.erase(myvector.begin()+position);
	std::cout << "current vector size: " << myvector.size() << std::endl;

	std::vector<int> somevector;
	for(int i =0; i<10; ++i) {
		somevector.push_back(i*2);
	}
	for(int i = 0; i<10; ++i) {
		if(somevector[i] == 6) {
			std::cout << i << std::endl;
		}
	}



	return 0;
}
