//============================================================================
// Name        : Deque(kolejka).cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <deque>
using namespace std;

int main() {
	  int myints[] = {16,2,77,29};
	  deque<int> first(myints, myints + sizeof(myints) / sizeof(int) );

	  cout << first.size() << endl;

	  first.push_front(20);
	  first.push_back(12);
	  for (std::deque<int>::iterator it = first.begin(); it!=first.end(); ++it){
	     cout << ' ' << *it << endl;
	  }
	  cout << first.size() << endl;


	return 0;
}
