//============================================================================
// Name        : Lista_jedno-dwukierunkowa.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;


struct Person {
    string name;
    string surname;
    int age;
    Person *next;
    Person();
};
Person::Person() {
    next = 0;
}

struct List {
    Person *first;
    void addPerson(string name, string surname, int age);
    void deleteLastPerson();
    void printList();
    List();
};
List::List() {
    first = 0;
}
void List::addPerson(string name, string surname, int age) {
    Person *newOne = new Person;
    newOne->name = name;
    newOne->surname = surname;
    newOne->age = age;

    if (first == 0) { //check first element of list, if = 0 ->new Person add like first position on list
        first = newOne;
    } else {
        Person *current = first;
        while (current->next) {
            current = current->next;
        }
        current->next = newOne;
        newOne->next = 0;
    }

}
void List::deleteLastPerson() {
    Person *current = first;
    Person *preLast;

    while (current->next != 0) {
        preLast = current;
        current = current->next;
    }
    delete current;
    preLast->next = 0;

}
void List::printList() {
    Person *temp = first;
    while (temp) {
        cout << "Name: " << temp->name << ", Surname: " << temp->surname
                << endl;
        temp = temp->next;
    }
}
int main() {
    List *baseList = new List;
//  List *first;
//  first = 0;
    baseList->addPerson("Mike", "Smith", 25);
    baseList->addPerson("Brad", "Pitt", 40);
    baseList->addPerson("James", "Bond", 38);
    cout << baseList->first->name << endl;
    cout << baseList->first->next->name << endl;
    cout << baseList->first->next->next->name << endl;
    baseList->printList();
    baseList->deleteLastPerson();
    baseList->deleteLastPerson();

    baseList->printList();

    delete baseList;
    return 0;
}

