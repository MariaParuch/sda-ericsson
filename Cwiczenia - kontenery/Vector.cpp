//============================================================================
// Name        : Vector.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>

int main ()
{
  // constructors used in the same order as described above:
  std::vector<int> first;                                // empty vector of ints
  std::vector<int> second (4,100);                       // four ints with value 100
  std::vector<int> third (second.begin(),second.end());  // iterating through second
  std::vector<int> fourth (third);                       // a copy of third

  // the iterator constructor can also be used to construct from arrays:
  int myints[] = {16,2,77,29};
  std::vector<double> sixth;
  std::vector<int> fifth (myints, myints + sizeof(myints) / sizeof(int) );

  std::cout << "The contents of fifth are:";			// petla z u�yciem iteratora
  for (std::vector<int>::iterator it = fifth.begin(); it != fifth.end(); ++it)
    std::cout << ' ' << *it;
  std::cout << '\n';

  std::cout << "vector 1: ";
	for (std::vector<int>::iterator i = first.begin(); i != first.end(); ++i)
		std::cout << ' ' << *i;
	  std::cout << '\n';

  std::cout << "vector 2: ";
	 for (std::vector<int>::iterator i = second.begin(); i != second.end(); ++i)
		 std::cout << ' ' << *i;
		std::cout << '\n';

  std::cout << "vector 3: ";
      for (std::vector<int>::iterator i = third.begin(); i != third.end(); ++i)
          std::cout << ' ' << *i;
        std::cout << '\n';

	std::cout << "vector 4: ";							// p�tla jak przy zwyk�ej tablicy
	  for (unsigned int i = 0; i < fourth.size(); ++i)
		  std::cout << ' ' << fourth[i];
		std::cout << '\n';

	std::cout << "Int max_size: " << first.max_size() << std::endl;
	std::cout << "Double max_size: " << sixth.max_size() << std::endl;
	int myint;
	std::cout << "Size of second: " << second.size() << std::endl;
	std::cout << "Give next number to vector" << std::endl;
	std::cin >> myint;
	second.push_back (myint);

	std::cout << "New size of second: " << second.size() << std::endl;
	std::cout << "push_back capacity: " << std::endl;
	for(int i = 0; i<20; ++i) {
		first.push_back(2);
		std::cout << "capacity: " <<  (int) first.capacity() << std::endl;
	}
	std::cout << "push_back max_size: " << first.max_size() << std::endl;
	std::cout << "pop_back capacity: " << std::endl;
	for (int i = 20; i>0; --i) {
		first.pop_back();
		std::cout << "capacity: " <<  (int) first.capacity() << std::endl;
	}
	std::cout << "pop_back max_size: " << first.max_size() << std::endl;

	std::vector<int> seventh (10);
	for(int i =0; i<seventh.size(); ++i) {
		seventh[i] = i;
		std::cout << ' ' << seventh.at(i);
		std::cout << '\n';
	}
//	std::cout << seventh[15] << std::endl;
//	std::cout << seventh.at(15) << std::endl;

 std::vector<int> myvector (3,100);
 std::vector<int>::iterator it;

	  it = myvector.begin();
	  it = myvector.insert ( it , 200 );

	  myvector.insert (it,2,300);
	  it = myvector.end();
	  myvector.insert (it, 500);
	  std::cout << "myvector contains:";
	    for (it=myvector.begin(); it<myvector.end(); it++)
	      std::cout << ' ' << *it;
	    std::cout << '\n';
	  myvector.erase(myvector.begin()+1);
	  std::cout << "myvector contains after erase:";
		for (it=myvector.begin(); it<myvector.end(); it++)
		  std::cout << ' ' << *it;
		std::cout << '\n';
 std::cout << "size and capacity before clear: " << myvector.size() << ", " << (int) myvector.capacity() << std::endl;
 myvector.clear();
 std::cout << "size and capacity after clear: " << myvector.size() << ", " << (int) myvector.capacity() << std::endl;
 myvector.reserve(100);
 std::cout << "size and capacity after reserve 100: " << myvector.size() << ", " << (int) myvector.capacity() << std::endl;

 std::vector<int> othervector (5,200);
 std::cout << "size and capacity: " << othervector.size() << ", " << (int) othervector.capacity() << std::endl;
 othervector.resize(7);
 std::cout << "size and capacity: " << othervector.size() << ", " << (int) othervector.capacity() << std::endl;
 for (unsigned int i = 0; i < othervector.size(); ++i)
 		  std::cout << ' ' << othervector[i];
 		std::cout << '\n';
 othervector.resize(3);
 std::cout << "size and capacity after resize to 3: " << othervector.size() << ", " << (int) othervector.capacity() << std::endl;
 for (unsigned int i = 0; i < othervector.size(); ++i)
 		  std::cout << ' ' << othervector[i];
 		std::cout << '\n';
 if (myvector.empty()) {
	 std::cout << "myvector is empty" << std::endl;
 } else {
	 std::cout << "myvector is not empty";
 };

 if (!othervector.empty()) {
 	 std::cout << "myvector is not empty";
  } else {
	  std::cout << "myvector is empty" << std::endl;
  }



  return 0;
}
