//============================================================================
// Name        : Mapa.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <map>

bool fncomp (char lhs, char rhs) {return lhs<rhs;}

struct classcomp {
  bool operator() (const char& lhs, const char& rhs) const
  {return lhs<rhs;}
};

int main ()
{
  std::map<char,int> first;

  first['a']=10;
  first['b']=30;
  first['c']=50;
  first['d']=70;
  first['e']= first['d'];


  std::cout << first['a'] << std::endl;
  std::cout << first['b'] << std::endl;
  std::cout << first['c'] << std::endl;
  std::cout << first['d'] << std::endl;
  std::cout << first['e'] << std::endl;

  first['e'] = 35;
  std::cout << first['d'] << std::endl;
  std::cout << first['e'] << std::endl;

  std::cout<< "Size of first map: " << first.size() << std::endl;

  std::map<char,int>::iterator it, itlow, itup;

  it = first.find('a');
  first.erase(it);
  first.erase('b');

  itlow=first.lower_bound ('b');
  itup=first.upper_bound ('d');

  std::cout << "lower: " << itlow->second << " key: " << itlow->first << std::endl;
  std::cout << "upper: " << itup->second << " key: " << itup->first << std::endl;
  std::cout<< "Size of first map after erase: " << first.size() << std::endl;

  it = first.find('c');
    if (it != first.end())
      first.erase (it);
  std::cout<< "Size of first map after next erase: " << first.size() << std::endl;

  char c;
  for (c='a'; c<'e'; c++)
  	  {
	std::cout << c;
	if (first.count(c)>0)
	  std::cout << " is an element of first map.\n";
	else
	  std::cout << " is not an element of first map.\n";
  	  }

  std::map<char,int> second (first.begin(),first.end());
  std::cout << second['a'] << std::endl;
  std::map<char,int> third (second);

  std::map<char,int,classcomp> fourth;                 // class as Compare

  bool(*fn_pt)(char,char) = fncomp;
  std::map<char,int,bool(*)(char,char)> fifth (fn_pt); // function pointer as Compare

  //multimapa

  std::multimap<char,int> mymm;

    mymm.insert (std::make_pair('z',30));
    mymm.insert (std::make_pair('z',40));

    std::cout << "z => " << mymm.find('z')->second << '\n';
    itup = mymm.upper_bound('z');
    std::cout << "z => " << itup->second << '\n';

  return 0;
}
