//============================================================================
// Name        : Lista.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <list>
#include <vector>
#include <cctype>

bool conditionFun (const int& value) { return (value%4 == 0); }


int main ()
{
  // constructors used in the same order as described above:
  std::list<int> first;                                // empty list of ints
  std::list<int> second (4,100);                       // four ints with value 100
  std::list<int> third (second.begin(),second.end());  // iterating through second
  std::list<int> fourth (third);                       // a copy of third

  // the iterator constructor can also be used to construct from arrays:
  int myints[] = {16,2,77,29};
  std::list<int> fifth (myints, myints + sizeof(myints) / sizeof(int) );

  std::cout << "The contents of fifth are: ";
  for (std::list<int>::iterator it = fifth.begin(); it != fifth.end(); it++)
    std::cout << *it << ' ';

  std::cout << '\n';

  std::cout << "list 1: ";
  	for (std::list<int>::iterator i = first.begin(); i != first.end(); ++i)
  		std::cout << ' ' << *i;
  	  std::cout << '\n';

    std::cout << "list 2: ";
  	 for (std::list<int>::iterator i = second.begin(); i != second.end(); ++i)
  		 std::cout << ' ' << *i;
  		std::cout << '\n';

    std::cout << "list 3: ";
        for (std::list<int>::iterator i = third.begin(); i != third.end(); ++i)
            std::cout << ' ' << *i;
          std::cout << '\n';

  	std::cout << "list 4: ";
  	  for (std::list<int>::iterator i = fourth.begin(); i != fourth.end(); ++i)
  		  std::cout << ' ' << *i;
  		std::cout << '\n';
  		std::cout << fourth.back() << std::endl;

	for (int i=1; i<9; ++i) {
		first.push_back(i);
	}
	first.resize(12);
	for (std::list<int>::iterator i = first.begin(); i != first.end(); ++i)
			  std::cout << ' ' << *i;
			std::cout << '\n';
	first.resize(5);
	for (std::list<int>::iterator i = first.begin(); i != first.end(); ++i)
			  std::cout << ' ' << *i;
			std::cout << '\n';

	std::list<int>::iterator it;
	it = first.begin();
	first.insert(it, 12);
	it++;
	first.insert(it, 2, 13);

	for (std::list<int>::iterator i = first.begin(); i != first.end(); ++i)
			  std::cout << ' ' << *i;
			std::cout << '\n';
	std::vector<int> myvector (2,14);
	  first.insert (it,myvector.begin(),myvector.end());

	for (std::list<int>::iterator i = first.begin(); i != first.end(); ++i)
			  std::cout << ' ' << *i;
			std::cout << '\n';

	std::list<int>::iterator it1;
	it = it1 = first.begin();
	advance (it1,4);
	std::cout << *it1 << std::endl;;
	first.erase(it1);
	it1 = it;
	advance (it1, 3);
	first.erase(it,it1);
	for (std::list<int>::iterator i = first.begin(); i != first.end(); ++i)
			  std::cout << ' ' << *i;
			std::cout << '\n';

	first.reverse();
	for (std::list<int>::iterator i = first.begin(); i != first.end(); ++i)
			  std::cout << ' ' << *i;
			std::cout << '\n';
	first.sort();
	for (std::list<int>::iterator i = first.begin(); i != first.end(); ++i)
			  std::cout << ' ' << *i;
			std::cout << '\n';

	int myList[] = {43, 2, 99, 33, 7, 11, 4, 111, 7, 99};
	std::list<int> sixth (myList, myList+10);

	for (std::list<int>::iterator i = sixth.begin(); i != sixth.end(); ++i)
			  std::cout << ' ' << *i;
			std::cout << '\n';
	sixth.sort();
	sixth.unique();

	for (std::list<int>::iterator i = sixth.begin(); i != sixth.end(); ++i)
			  std::cout << ' ' << *i;
			std::cout << '\n';
	sixth.remove(43);
	for (std::list<int>::iterator i = sixth.begin(); i != sixth.end(); ++i)
			  std::cout << ' ' << *i;
			std::cout << '\n';
	second.remove(100);
	std::cout << "list 2: ";
	for (std::list<int>::iterator i = second.begin(); i != second.end(); ++i)
	  		 std::cout << ' ' << *i;
	  		std::cout << '\n';
	sixth.remove_if(conditionFun);
	for (std::list<int>::iterator i = sixth.begin(); i != sixth.end(); ++i)
			  std::cout << ' ' << *i;
			std::cout << '\n';

	std::list<int> mylist1, mylist2;
	std::list<int>::iterator is;

	  for (int i=1; i<=4; ++i)
		 mylist1.push_back(i);      // mylist1: 1 2 3 4

	  for (int i=1; i<=3; ++i)
		 mylist2.push_back(i*10);   // mylist2: 10 20 30

	  is = mylist1.begin();
	  ++is;                        // points to 2

	  mylist1.splice(is,mylist2);// mylist1: 1 10 20 30 3 4
	  mylist2.splice(mylist2.begin() ,mylist1, is);  // mylist2: 2


	 std::list<double> one, two;

	   one.push_back (3.1);
	   one.push_back (2.2);
	   one.push_back (2.9);

	   two.push_back (3.7);
	   two.push_back (7.1);
	   two.push_back (1.4);

	   one.sort();
	   two.sort();
	   one.merge(two);
	   for (std::list<double>::iterator i = one.begin(); i != one.end(); ++i)
	   			  std::cout << ' ' << *i;
	   			std::cout << '\n';

	 std::list<int> seventh, eight;
	 for(int i=1; i<15; ++i){
		 seventh.push_back(i);
	 }
	 it = it1 = seventh.begin();
	 seventh.reverse();
//	 seventh.sort(compare_nocase);
//	 for (std::list<int>::iterator i = seventh.begin(); i != seventh.end(); ++i)
//	 	   			  std::cout << ' ' << *i;
//	 	   			std::cout << '\n';

//	 advance (it,2);
//	 advance (it1, 8);
//	 eight.splice(eight.begin(), seventh, it, it1);
//
//	 for (std::list<int>::iterator i = eight.begin(); i != eight.end(); ++i)
//	 			  std::cout << ' ' << *i;
//	 			std::cout << '\n';


  return 0;
}
