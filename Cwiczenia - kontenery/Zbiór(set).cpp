//============================================================================
// Name        : Zbi�r(set).cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <set>

bool fncomp (int lhs, int rhs) {return lhs<rhs;}

struct classcomp {
  bool operator() (const int& lhs, const int& rhs) const
  {return lhs<rhs;}
};

int main ()
{
  std::set<int> first;                           // empty set of ints

  int myints[]= {10,20,30,30,40,50};
  std::set<int> second (myints,myints+6);        // range
  for(std::set<int>::iterator i = second.begin(); i!= second.end(); ++i) {
	  std::cout << " " << *i << std::endl;
  }
  std::cout<< "Set size: " << second.size() << std::endl;
  second.insert(21);
  std::cout<< "Set size: " << second.size() << std::endl;

  std::set<int>::iterator it;
  it = second.begin();
  advance (it, 3);
  second.erase(it, second.end());
  std::cout<< "Set size after erase: " << second.size() << std::endl;
  std::set<int> third (second);                  // a copy of second

  std::set<int> fourth (second.begin(), second.end());  // iterator ctor.

  std::set<int,classcomp> fifth;                 // class as Compare

  bool(*fn_pt)(int,int) = fncomp;
  std::set<int,bool(*)(int,int)> sixth (fn_pt);  // function pointer as Compare

  return 0;
}
