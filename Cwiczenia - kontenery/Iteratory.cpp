//============================================================================
// Name        : Iteratory.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>     // std::cin, std::cout
#include <iterator>
#include <vector>       // std::vector
#include <algorithm>  // std::istream_iterator
#include <cmath>

int main () {
//  double value1, value2;
//  std::cout << "Please, insert two values: ";
//
//  std::istream_iterator<double> eos;              // end-of-stream iterator
//  std::istream_iterator<double> iit (std::cin);   // stdin iterator
//
//  if (iit!=eos) value1=*iit;
//
//  ++iit;
//  if (iit!=eos) value2=*iit;
//
//  std::cout << value1 << "*" << value2 << "=" << (value1*value2) << '\n';
//
////***************** OSTREAM *******************
//
//  std::vector<int> myvector;
//  	  for (int i=1; i<10; ++i) myvector.push_back(i*10);
//
//  std::ostream_iterator<int> out_it (std::cout,", ");
//  std::copy ( myvector.begin(), myvector.end(), out_it );

  std::vector<int> first;
  std::vector<int>::iterator i;
   for(int j = 0; j<10; j++) {
	   first.push_back(j);
   }
   i = first.begin();
   advance (i, 7);
   for(;i!=first.end(); i++) {
	   *i = pow(*i,2);
   }
   for (std::vector<int>::iterator it = first.begin(); it!= first.end(); ++it) {
	   std::cout << *it << " ";
   }
   for (std::vector<int>::reverse_iterator rit = first.rbegin(); rit!= first.rend(); ++rit){
	   std::cout << *rit << " ";
   }

   std::vector<int> second;
   std::vector<int> third;
   for(int j = 0; j<15; j++) {
  	   second.push_back(j+100);
  	   third.push_back(j);
     }

   // ******* copy by using simple iterator

//   std::vector<int>::iterator itt;
//   itt = third.begin();
//   advance (itt,6);
//   std::cout<< std::endl;
//   third.resize(30);
//   std::copy(second.begin(), second.end(), itt);


   // ******* copy by using insert iterator

   std::vector<int>::iterator itt = third.begin();
   advance(itt,6);
   std::insert_iterator< std::vector<int> > insert_itt(third,itt);
   std::copy(second.begin(), second.end(), insert_itt);
   std::cout<<std::endl;
   for(std::vector<int>::iterator o = third.begin(); o!= third.end(); ++o) {
   	   std::cout << *o << " ";
   }

  return 0;
}
