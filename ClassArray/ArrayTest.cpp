/*
 * ArrayTest.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */
#include "gtest.h"
#include "Array.h"
#include <iostream>

class ArrayTest : public testing:: Test {
protected:
	Array array;
	};

TEST_F(ArrayTest, tabSetCorrectly) {
	 array.fillArray();
	 EXPECT_EQ(0, array.getElement(0));
	 EXPECT_EQ(-1, array.getElement(-1));
	 EXPECT_EQ(-1, array.getElement(1001));
	 EXPECT_EQ(54, array.getElement(54));
	 EXPECT_EQ(999, array.getElement(999));
	 EXPECT_EQ(1000, array.getCurrentArraySize());
}
TEST_F(ArrayTest, currentTabSize) {
	 array.fillArrayWithOnes();
	 EXPECT_EQ(1, array.getElement(0));
	 EXPECT_EQ(1, array.getElement(9));
}
TEST_F(ArrayTest, tabPtrSize) {
	array.fillArray();
	EXPECT_EQ(0, array.getPtr());
	EXPECT_NE(11,array.getPtr());
}
TEST_F(ArrayTest, addLastElement) {
	array.fillArrayWithOnes();
	array.addLastElement(5);
	EXPECT_EQ(5, array.getElement(10));
	EXPECT_EQ(11, array.getCurrentArraySize());
}
TEST_F(ArrayTest, removeLastElement) {
	array.fillArray();
	array.removeLastElement();
	EXPECT_EQ(-1, array.getElement(1000));
	EXPECT_EQ(999, array.getCurrentArraySize());
}
TEST_F(ArrayTest, addElementRandomly) {
	array.fillArrayToTen();
	EXPECT_EQ(10, array.getCurrentArraySize());
	EXPECT_EQ(9,array.getElement(9));
	array.addElementForIndex(12,5);
	EXPECT_EQ(11, array.getCurrentArraySize());
	EXPECT_EQ(12, array.getElement(5));
	EXPECT_EQ(7, array.getElement(8));
	EXPECT_EQ(1, array.getElement(1));
	EXPECT_EQ(9, array.getElement(10));
}
TEST_F(ArrayTest, removeElementRandomly) {
	array.fillArrayToTen();
	array.removeElementFromIndex(5);
	EXPECT_EQ(9, array.getCurrentArraySize());
	EXPECT_EQ(6, array.getElement(5));
	EXPECT_EQ(9, array.getElement(8));
	EXPECT_EQ(-1, array.getElement(9));
}
TEST_F(ArrayTest, maxElement) {
	array.fillArrayToTen();
	EXPECT_EQ(9, array.getMaxElement());
}
TEST_F(ArrayTest, minElement) {
	array.fillArrayToTen();
	EXPECT_EQ(0, array.getMinElement());
}
TEST_F(ArrayTest, addingElements) {
	array.fillArrayWithOnes();
	EXPECT_EQ(10, array.addElements());
}






