/*
 * Array.h
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#ifndef ARRAY_H_
#define ARRAY_H_
#include <iostream>

const int maxArraySize = 1000;

class Array {
public:
	Array();
	virtual ~Array();
	void fillArray();
	void fillArrayWithOnes();
	void fillArrayToTen();
	void fillArrayRandomly();
	bool checkArrayIndex(int index);
	float getElement(int index);
	int getCurrentArraySize() const {return currentArraySize;}
	float getPtr();
	void addLastElement(int element);
	void removeLastElement();
	void addElementForIndex(int element, int index);
	void removeElementFromIndex(int index);
	float getMaxElement();
	float getMinElement();
	float addElements();

private:
	float array[maxArraySize];
	int currentArraySize;

};

#endif /* ARRAY_H_ */
