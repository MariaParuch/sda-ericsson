/*
 * Array.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#include "Array.h"
#include <cstdlib>
#include <time.h>

Array::Array(){
	currentArraySize = 0;
}
Array::~Array() {
}
bool Array::checkArrayIndex(int index)
{
	if (index <0 || index >= currentArraySize) {
		return false;
	}
	return true;
}
void Array::fillArray() {
	for (int i=0; i<maxArraySize; i++){
			array[i] = i;
			currentArraySize++;
	}
}
void Array::fillArrayWithOnes() {
	for (int i = 0; i<10; i++) {
		array[i] = 1;
		currentArraySize++;
		}
}
void Array::fillArrayToTen() {
	for (int i = 0; i<10; i++) {
		array[i] = i;
		currentArraySize++;
	}
}
void Array::fillArrayRandomly() {
	srand (time(NULL));
	for (int i=0; i<=15; i++) {
		array[i] = rand() % 20;
		currentArraySize++;
		std::cout << array[i] << ", ";
	}
}
float Array::getElement(int index)
{
	if (checkArrayIndex(index)) {
		return array[index];
	} else {
		return -1;
	}
}
float Array::getPtr()
{
	float ptr = *array;
	return ptr;
}
void Array::addLastElement(int element) {
	if (currentArraySize < maxArraySize) {
		array[currentArraySize] = element;
		currentArraySize++;
	}
}
void Array::removeLastElement() {
	currentArraySize--;
	array[currentArraySize] = 0;
}
void Array::addElementForIndex(int element, int index) {
	if (currentArraySize <= maxArraySize) {
		for(int i = currentArraySize; i>index; i--) {
			array[i] = array[i-1];
		}
		currentArraySize++;
		array[index] = element;
		}
}
void Array::removeElementFromIndex(int index) {
	if(index<currentArraySize-1) {
		for(int i = index; i<currentArraySize; i++) {
				array[i] = array[i+1];
			}
		currentArraySize--;
		} else {
			currentArraySize--;
		}
}
float Array::getMaxElement() {
	int max = array[0];
	for(int i=0; i<currentArraySize; ++i){
		if (array[i]>max) {
			max = array[i];
		}
	}
	return max;
}
float Array::getMinElement() {
	int min = array[0];
	for(int i=0; i<currentArraySize; ++i){
		if (array[i]<min) {
			min = array[i];
		}
	}
	return min;
}
float Array::addElements() {
	float sum = 0;
	for(int i=0; i<currentArraySize; i++){
		int temp = array[i];
		sum += temp;
	}
	return sum;
}

