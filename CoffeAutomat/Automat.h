/*
 * Automat.h
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef AUTOMAT_H_
#define AUTOMAT_H_
#include <iostream>
#include <string>
const int countOfProducts = 10;

struct Product {
	Product() {};
	Product(std::string name, double price)
	{
		this-> name = name; this-> price = price;
	}
	std::string name;
	double price;

};

class Automat {
public:
	Automat();
	virtual ~Automat();
	void getMenu();
	void chooseElement();
	void pay(Product *drink);



private:
	Product listOfProduct[countOfProducts];


};

#endif /* AUTOMAT_H_ */
