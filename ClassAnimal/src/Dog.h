/*
 * Dog.h
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef DOG_H_
#define DOG_H_

#include "Animal.h"
#include <string>

class Dog: virtual public Animal {
public:
	Dog(std::string givenName);
	virtual ~Dog();
	void giveASound();
	void getMove();
//		using Animal::speak;
//		void speak();
//		void speak(double no);
};

#endif /* DOG_H_ */
