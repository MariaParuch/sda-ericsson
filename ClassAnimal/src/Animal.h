/*
 * Animal.h
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef ANIMAL_H_
#define ANIMAL_H_
#include <string>

class Animal {
public:
	Animal(std::string givenName);
	virtual ~Animal();
	void speak();
	std::string giveName()
{
		return std::string("my name is: ") + name;
}
	void move();
//	void speak(int no);
private:
	virtual void giveASound() = 0;
	virtual void getMove() = 0;
	std::string name;
};

#endif /* ANIMAL_H_ */
