/*
 * Animal.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#include "Animal.h"
#include <iostream>
#include <string>

Animal::Animal(std::string givenName):
		name(givenName)
{

}
Animal::~Animal(){
	std::cout << "Destroying animal" << std::endl;
}
void Animal::speak()
{
	std:: cout << name << "speaks: ";
	giveASound();
	std::cout << std::endl;
}
void Animal::move()
{
	getMove();
}
//void Animal::speak(int no)
//{
//	std:: cout << "animal speaks! " << std:: endl;
//}

