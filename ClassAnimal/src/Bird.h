/*
 * Bird.h
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef BIRD_H_
#define BIRD_H_

#include "Animal.h"
#include <string>

class Bird: public Animal {
public:
	Bird(std::string giveASound);
	void giveASound();
	void getMove();
};

#endif /* BIRD_H_ */
