/*
 * Dog.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#include "Dog.h"
#include <iostream>
#include <string>

//void Dog::speak()
//{
//	std::cout << "hauu!" << std::endl;
//}
//void Dog::speak(double no)
//{
//	std::cout << "hauu!" << std::endl;
//}
Dog::Dog(std::string givenName):Animal(givenName){
	std::cout << "constructing dog" << std::endl;
}

void Dog::giveASound()
{
	std::cout << "hauu!";
}
void Dog::getMove()
{
	std:: cout << "It runs! ";
}
Dog::~Dog(){
	std:: cout << "destroying dog" << std::endl;
}
