/*
 * Classroom.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "Classroom.h"


Classroom::Classroom() {
    this->numClassroom = -1;
    this->capacity = -1;
    this->building = ("");
    numCoursesInRoom = 0;
}
Classroom::Classroom(int numClassroom, int capacity, std::string building) {
    this->numClassroom = numClassroom;
    this->capacity = capacity;
    this->building = building;
    numCoursesInRoom = 0;
}

void Classroom::addCourse(Course* newCourse) {
    if (numCoursesInRoom < MAX_NUM_COURSES_IN_ROOM) {
        courseList[numCoursesInRoom] = newCourse;
        numCoursesInRoom++;
        std::cout << "Add completed" << std::endl;
    } else {
        std::cout << "Sorry. This classroom is full. " << std::endl;
    }
}

