/*
 * Cours.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */


#ifndef COURSE_H_
#define COURSE_H_
#include <string>
#include "Classroom.h"
//#include "Student.h"
//#include "Teacher.h"
const int MAX_NUMBER_OF_STUDENTS = 30;
const int MAX_COURSES = 10;

class Student;
class Teacher;

class Course {
    public:
        Course() {};
        Course(std::string topic);
        void addStudent(Student *participant);
        std::string& getTopic() {return topic;}

    private:
        Classroom room;
        int endDate;
        int startDate;
        Student *studentsList[MAX_NUMBER_OF_STUDENTS];
        Teacher *teachersList[];
        std::string topic;
        std::string describe;
        int seats;
        int freeSeats;
        int numberOfStudents;

};

#endif /* COURSE_H_ */
