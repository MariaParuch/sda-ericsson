/*
 * Person.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef PERSON_H_
#define PERSON_H_

#include <string>
#include <iostream>

#include "Course.h"

class Person {
    public:
        Person(std::string firstName, std::string lastName, std::string email,
            int age);
    //  void describe();
        void addCourse(Course* course);
        Course** getYourCourses() {return yourCourseList;};
        void showMyCourse();

    protected:
        std::string firstName;
        std::string lastName;
        std::string email;
        int age;
        int numCourses;
        Course* yourCourseList[MAX_COURSES];

};

#endif /* PERSON_H_ */
