/*
 * Classroom.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef CLASSROOM_H_
#define CLASSROOM_H_

#include <string>
#include <iostream>

class Course;

const int MAX_NUM_COURSES_IN_ROOM = 5;

class Classroom {
    public:
        Classroom();
        Classroom(int numClassroom, int capacity, std::string building);
        void addCourse(Course* nameCours);
        //const Course** getCoursList(){return courseList;};
        Course** getCoursList(){return courseList;};
        int getNumCoursesInRoom() {return numCoursesInRoom;};
    //  void describe();
        std::string getBuilding() const {return building;}
        void setBuilding(std::string building) {this->building = building;}
        int getCapacity() const {return capacity;}
        void setCapacity(int capacity) {this->capacity = capacity;}
        int getNumClassroom() const {return numClassroom;}
        void setNumClassroom(int numClassroom) {this->numClassroom = numClassroom;}

    private:
        int numClassroom, capacity;
        std::string building;
        int numCoursesInRoom;
        Course *courseList[MAX_NUM_COURSES_IN_ROOM];
};

#endif /* CLASSROOM_H_ */
