/*
 * Teacher.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef TEACHER_H_
#define TEACHER_H_

#include "Person.h"

class Teacher: public Person {
public:
    Teacher(std::string firstName, std::string lastName, std::string email, int age);

};

#endif /* TEACHER_H_ */
