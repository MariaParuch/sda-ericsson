//============================================================================
// Name        : ClassCours.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
#include "Classroom.h"
#include "Person.h"
#include "Teacher.h"
#include "Student.h"
#include "Course.h"

int main() {

    Course courseList[10];
    courseList[0] = Course ("English");
    courseList[1] = Course ("German");
    courseList[2] = Course ("French");
    courseList[3] = Course ("Spanish");
    courseList[4] = Course ("Norwegian");
    courseList[5] = Course ("Chinese");
    courseList[6] = Course ("Indian");
    courseList[7] = Course ("Russian");
    courseList[8] = Course ("Arabic");
    courseList[9] = Course ("Hungarian");

// print menu
//  int option=0;
    string firstName;
    string lastName;
    string email;
    int age;

//  cout << "Choose option: " << endl;
//  cout << "1. Add student " << endl;
//  cout << "2. Print all courses " << endl;
//  cout << "3. Add courses " << endl;
//  cout << "0. Exit " << endl;
    cout << "Hello! Welcome in our school! Please, write your data to enroll " << endl;
    cout << "Your first name: ";
    cin >> firstName;
    cout << "Your last name: ";
    cin >> lastName;
    cout << "Your email: ";
    cin >> email;
    cout << "Your age: ";
    cin >> age;

    Student firstStudent(firstName, lastName, email, age);
//  after create newStudent, next step is the course choice
    cout << "Your profile is ready. Our courses: " << endl;

    for (int i = 0; i<MAX_COURSES; i++){
        cout << i+1 << ". " << courseList[i].getTopic() << endl;
    }
    int coursePosition;
    cout << "Choose the course (from 1 to 10): ";
    cin >> coursePosition;
    coursePosition--;
    while (coursePosition <0 || coursePosition >9) {
        cout << "Your choice is out of range. Please, select correctly. " << endl;
    }
    courseList[coursePosition].addStudent(&firstStudent);
    firstStudent.addCourse(&courseList[coursePosition]);
    firstStudent.showMyCourse();

//    int nextStep;
//    cout << "Next course = 1, Exit = 2";
//    cin >> nextStep;
//    if (nextStep == 1){
//        for (int i = 0; i<MAX_COURSES; i++){
//            cout << i+1 << ". " << courseList[i].getTopic() << endl;
//        }
//        int coursePosition;
//        cout << "Choose the course (from 1 to 10): ";
//        cin >> coursePosition;
//        coursePosition--;
//        while (coursePosition <0 || coursePosition >9) {
//            cout << "Your choice is out of range. Please, select correctly. " << endl;
//        }
//        courseList[coursePosition].addStudent(&firstStudent);
//        firstStudent.addCourse(&courseList[coursePosition]);
//        firstStudent.showMyCourse();
//
//    }

    return 0;
}


