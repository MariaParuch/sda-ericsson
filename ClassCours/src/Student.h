/*
 * Student.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef STUDENT_H_
#define STUDENT_H_

#include "Person.h"


class Student: public Person {
public:
    Student(std::string firstName, std::string lastName, std::string email, int age);

};

#endif /* STUDENT_H_ */
