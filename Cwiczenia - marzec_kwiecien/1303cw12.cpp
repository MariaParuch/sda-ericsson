//============================================================================
// Name        : 1303cw12.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Duplicates in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

const int max_array_size = 5;
// WYPISUJE DUPLIKATY LICZB
int main() {
	int t[max_array_size] = {1,2,3,4,3};
	bool duplicate = false;

	for(int i=0; i<max_array_size; ++i){
		int a = 0;
		for(int j = 0; j<max_array_size; ++j){
		if (t[i] != t[j]) {
			duplicate = true;
			a++;
		}

	}
		if (duplicate == true && a>1){
			cout << t[i] << "powtarza sie: " << a << " razy";
				}
}
	return 0;
}
