//============================================================================
// Name        : Fibonacci.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Wypisujemy liczbe Fibonacciego" << endl;
	int n; // ilosc elementow ciagu F
	cout << "Podaj ktory element ciagu chcesz uzyskac" << endl;
	cin >> n;
	int n1 = 1;

	if (n == 1 || n == 2){
		cout << n1 << endl;
	} else {
		int n1 = 1;
		int n2 = 1;

		for(int i=3; i <= n; ++i){
			int temp = n1+n2;
			n1 = n2;
			n2 = temp;
	   }
		cout <<	n2 << endl;
	}

	return 0;
}
