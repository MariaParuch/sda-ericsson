//============================================================================
// Name        : Silnia.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Obliczamy silnie. Wpisz liczbe naturalna" << endl;
	int l; // podana liczba
	cin >> l;
	int silnia = 1;

	if (l == 0) {
		silnia = 1;
		cout << silnia << endl;
	} else {
		for (int i = 1; i <= l; ++i) {
			silnia = silnia * i;
		}
		cout << silnia << endl;
	}
	return 0;
}
