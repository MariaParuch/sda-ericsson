//============================================================================
// Name        : LiczbyPierwsze.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Podaj przedzia� liczbowy, w kt�rym wypiszemy liczby pierwsze " << endl;
	int min;
	int max;
	int i = 2;
	cin >> min >> max;
	if (min > max) {
		int temp = min;
		min = max;
		max = temp;
	}

	for(int liczba = min; liczba <=max; liczba ++) {
		bool liczbaPierwsza = true;
		for(i=2; i<=liczba; i ++) {
			if (liczba % i == 0 && liczba != i) {
				liczbaPierwsza = false;
				break;
			}
		}
		if (liczbaPierwsza && liczba > 1) {
			cout << "Liczby pierwsze w tym zakresie to: " << liczba << endl;
		}
	}
	return 0;
}
