//============================================================================
// Name        : ZgadywanieLiczby.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <time.h>
#include <cstdlib>
#include <windows.h>
using namespace std;

int main() {
	srand (time(NULL));
	int liczba = rand() % 20 + 1;
	int proba;
	int count = 1;
	cout << "Jaka liczba zosta�a wylosowana w zakresie od 1 do 20? wpisz swoj typ " << endl;
	cin >> proba;
	while (proba != liczba) {
		cout << "Podana liczba jest bledna - wpisz ponownie" << endl;
		cin >> proba;
		count ++;
	}
	cout << "Brawo! Ta liczba zostala wylosowana," << endl;
	Sleep(1000);
	cout << "Zgad�es za " << count << " razem." << endl;

	return 0;
}
