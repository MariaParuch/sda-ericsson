//============================================================================
// Name        : ObliczaniePola.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <math.h>
using namespace std;

int main() {
	cout << "Wybierz z menu figure, dla ktorej chcesz obliczyc pole i objetosc:" << endl;
	cout << "1. Szescian." << endl;
	cout << "2. Prostopadloscian." << endl;
	cout << "3. Kula." << endl;
	cout << "4. Ostroslup prawidlowy trojkatny." << endl;
	cout << "5. Walec." << endl;
	cout << "6. Zakoncz." << endl;

	int opcja;
	const double pi = 3.14;
	cin >> opcja;

	switch (opcja) {
	case 1:
		double a;
		cout << "Podaj dlugosc boku";
		cin >> a;
		cout << "Pole tego szescianu wynosi: " << 6*(a*a) << endl;
		cout << "Objetosc tego szescianu wynosi: " << (a*a*a) << endl;
		break;
	case 2:
		double b;
		double c;
		cout << "Podaj dlugosc krawedzi";
		cin >> a >> b >> c;
		cout << "Pole tego prostopadloscianu wynosi: " << 2*((a*b)+(b*c)+(c*a)) << endl;
		cout << "Objetosc tego prostopadloscianiu wynosi: " << (a*b*c) << endl;
		break;
	case 3:
		double r;
		cout << "Podaj promien kuli";
		cin >> r;
		cout << "Pole tej kuli wynosi: " << 4*pi*(r*r) << endl;
		cout << "Objetosc tej kuli wynosi: " << (4*pi*(r*r*r))/3 << endl;
		break;
	case 4:
		double h;
		double H;
		cout << "Podaj bok i wysokosc trojkata";
		cin >> a >> h;
		cout << "Pole tego ostroslupa wynosi: " << (((a*a)*(sqrt(3))/4) + ((3*a*h)/2)) << endl;
		cout << "Podaj wysokosc ostroslupa";
		cin >> H;
		cout << "Objetosc tego ostroslupa wynosi: " << ((a*a)*H*(sqrt(3))/12) << endl;
		break;
	case 5:
		cout << "Podaj promien podstawy walca i jego wysokosc";
		cin >> r >> h;
		cout << "Pole tego walca wynosi: " << 2*pi*r*(r+h) << endl;
		cout << "Objetosc tego walca wynosi: " << pi*(r*r)*h << endl;
		break;
	default:
		cout << "Do zobaczenia";
		break;
	}
	return 0;
}
