//============================================================================
// Name        : 1303cw2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================


/////////////////Program wczytuj�cy podan� ilo�� liczb i wypisuj�cy je na ekran w odwrotnej kolejno�ci
#include <iostream>
using namespace std;

const int max_array_size = 5;

int main() {
	int t[max_array_size];
	cout << "Podaj 5 dowolnych liczb calkowitych" << endl;
	for(int i=0; i<max_array_size; ++i){
		cin >> t[i];
	}
	cout << "Wypisuje je w odwrotnej kolejnosci" << endl;
	for(int i=max_array_size-1; i>=0; --i){
		cout << t[i] << " ";
	}
	return 0;
}
