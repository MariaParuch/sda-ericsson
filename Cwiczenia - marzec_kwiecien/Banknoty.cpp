//============================================================================
// Name        : Banknoty.cp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

/////////////Program wypisuj�cy minimaln� ilo�� banknot�w ka�dego nomina�u potrzebnych do stworzenia podanej kwoty

#include <iostream>
using namespace std;

int main() {
	cout << "Podaj kwote (w dziesi�tkach zlotych)" << endl;
	int kwota;
	cin >> kwota;

	if (kwota >= 500) {
		int liczbaBanknotow500 = kwota/500;
		kwota = kwota % 500;
		cout << "Ilosc banknotow 500 to: " << liczbaBanknotow500 << endl;
	}
	if (kwota >=200) {
		int liczbaBanknotow200 = kwota/200;
		kwota = kwota % 200;
		cout << "Ilosc banknotow 200 to: " << liczbaBanknotow200 << endl;
	}
	if (kwota>=100) {
		int liczbaBanknotow100 = kwota/100;
		kwota = kwota % 100;
		cout << "Ilosc banknotow 100 to: " << liczbaBanknotow100 << endl;
	}
	if (kwota>=50) {
		int liczbaBanknotow50 = kwota/50;
		kwota = kwota % 50;
		cout << "Ilosc banknotow 50 to: " << liczbaBanknotow50 << endl;
	}
	if (kwota>=20) {
		int liczbaBanknotow20 = kwota/20;
		kwota = kwota % 20;
		cout << "Ilosc banknotow 20 to: " << liczbaBanknotow20 << endl;
	}
	if (kwota>=10) {
		int liczbaBanknotow10 = kwota/10;
		kwota = kwota % 10;
		cout << "Ilosc banknotow 10 to: " << liczbaBanknotow10 << endl;
	}
	return 0;
}
