//============================================================================
// Name        : 1303cw7.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

///////////////Program wypisujący liczby parzyste z tablicy

#include <iostream>
using namespace std;

const int max_array_size = 5;
// wypisuje liczby parzyste
int main() {
	int t[max_array_size] = {0,6,8,9,1};
	const int *it = t;
	cout << "Liczby parzyste w tablicy to: " << endl;
	for(int i=0; i<max_array_size; ++i){
//		if (t[i]% 2 == 0) {
//		cout << t[i] << endl;
		if ((*it)%2 == 0) {
			cout << *it << " ";
		}
		it++;
	}
	return 0;
}
