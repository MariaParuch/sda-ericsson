/*
 * Id.h
 *
 *  Created on: 11.05.2017
 *      Author: RENT
 */

#ifndef ID_H_
#define ID_H_

class Id {
public:
	Id();
	virtual ~Id();
	int idNumber;
//	void setIdNumber(int idNumber) {this->idNumber = idNumber;}

private:
	static int lastIdNumber;

};

#endif /* ID_H_ */
