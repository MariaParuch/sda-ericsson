//============================================================================
// Name        : GeneratorId.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Id.h"
using namespace std;

int main() {
	Id groupObject[10];
	for(int i = 0; i<10; i++) {
		cout << i << " id num: " << groupObject[i].idNumber << endl;
	}

	return 0;
}
//1. Stworzy� klas�, kt�ra ka�demu nowoutworzonemu obiektowi tej klasy nada unikalny numer identyfikacyjny.
//2. Doda� mo�liwo�� ustawiania od jakiej warto�ci nadajemy identyfikatory. [ilosc znakow: 5: zakres liczbowy od 10 - 100, litera 1
//3. Pozwoli� by identyfikator by� �a�cuchem znak�w, nie tylko liczb�.
