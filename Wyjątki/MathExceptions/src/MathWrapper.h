/*
 * MathWrapper.h
 *
 *  Created on: 18.05.2017
 *      Author: RENT
 */

#ifndef MATHWRAPPER_H_
#define MATHWRAPPER_H_
#include <math.h>
#include <iostream>

class MathWrapper {
public:
	class edomException : public exception {
		virtual const char* what() const throw() {
			return "EDOM exception";
		}
	};

	class erangeException : public exception {
		virtual const char* what() const throw() {
			return "ERANGE exception";
		}
	};

	double myLog(double x);
	double myPow(double x, double y);
	double myCos(double x);
	double mySin(double x) {
			double result = sin(x*PI/180);
			cout << "result: " << result << endl;
			return result;
		}

	double myTan(double x) {
				double result = tan(x*PI/180);
				cout << "result: " << result << endl;
				return result;
			}

};

#endif /* MATHWRAPPER_H_ */
