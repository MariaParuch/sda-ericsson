//============================================================================
// Name        : Wyj�tki4.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <math.h>
#include <exception>
using namespace std;

class edomException : public exception {
	virtual const char* what() const throw() {
		return "EDOM exception";
	}
};

class erangeException : public exception {
	virtual const char* what() const throw() {
		return "ERANGE exception";
	}
};

double myPow(double x, double y) {
	double result = pow(x,y);
	if (errno == EDOM){
		edomException e;
		throw e;
	}
	if(errno == ERANGE) {
		erangeException e;
		throw e;
		}
	return result;
}

double myLog(double x) {

	double result = log(x);
		if (errno == EDOM){
			edomException e;
			throw e;
		}
		if(errno == ERANGE) {
			erangeException e;
			throw e;
			}
		return result;
}
int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!

//	int a = pow(3.0,1000000000);
//	int b = log(-10);
//	if (errno == EDOM){
//		cout << "EDOM" << endl;
//	}
//	if(errno == ERANGE) {
//		cout << "ERANGE" << endl;
//	}
//	cout << "a: " << a << endl;

	try {
		int x = myPow(-3,1);
		int y = myLog(7);

	}
	catch(exception& e) {
		cout << "exception: " << e.what() << endl;
	}
	return 0;
}
