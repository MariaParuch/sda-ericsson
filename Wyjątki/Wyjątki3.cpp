//============================================================================
// Name        : Wyj�tki3.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;


int *function1(int n) {

	int *array = new int[n];
	return array;
}

int *function2( int n) {

	int *array = new (nothrow) int[n];
	return array;
}
int main() {

	int *firstArray;
	int *secondArray;
	int n = 30;
	int m = 40;

	try {
		firstArray = function1(n);
		secondArray = function2(m);
		if (!secondArray) {
			bad_alloc e;
			throw e;
		}
	}
	catch(bad_alloc& e) {
		cout << "Wyj�tek przy alokacji" << endl;
	}
	catch(...) {
		cout << "unrecognize exception" << endl;
	}
	return 0;
}
