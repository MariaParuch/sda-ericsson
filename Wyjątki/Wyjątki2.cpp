//============================================================================
// Name        : Wyj�tki2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <typeinfo>
#include <fstream>
#include <string>
using namespace std;

class Foo {
public:
	virtual ~Foo() {};
};

class Bar {
public:
	virtual ~Bar() {};
};
void displayException(exception& e) {
	cout << "exception: " << e.what() << endl;
}

void badException() throw(bad_exception) {
	bad_exception e;
	throw e;
}
int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
	Bar b;
	Bar *c = 0;
	try {
//		int *test = new int[10000000000]; // bad_alloc
//		int *test = new (nothrow)int[10000000000]; // bez wyj�tk�w
//		if(test == 0) { // spr dla wersji bez wyj�tk�w
//			cout << "nullptr" << endl;

//		Foo &f = dynamic_cast<Foo&>(b); // bad_cast

//		badException(); // bad_exception

//		cout << typeid(*c).name() << endl; // bad_typeid

//		ifstream f("moj plik nie istnieje");
//		f.exceptions(f.failbit);

//		string text; // out of range error
//		text.at(2);

		string x; // logic_error
		while(true) {
			x.resize(x.max_size()+1);
		}
	}
	catch(bad_alloc& e){
		displayException(e);
	}
	catch(bad_cast& e) {
		displayException(e);
	}
	catch(bad_exception& e) {
		displayException(e);
	}
	catch(bad_typeid& e) {
		displayException(e);
	}
	catch(ios_base::failure& e) {
		displayException(e);
	}
	catch(logic_error& e) {
		//invalid_argument
		//domain_error
		//length_error

		cout << "logic error" << endl;
		displayException(e);
	}
	catch(length_error& e) {
		cout << "length error" << endl;
	}
	catch(out_of_range& e) {
		displayException(e);
	}
	catch(...) {
		cout << "unrecognized exception" << endl;
	}
	return 0;
}
