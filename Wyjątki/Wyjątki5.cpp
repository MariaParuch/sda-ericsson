//============================================================================
// Name        : Wyj�tki5.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <exception>
using namespace std;

class myArray {
public:
	class memoryException : public exception {
		virtual const char* what() const throw() {
			return "memory exception";
		}
	};
	myArray(int size = 0) {
		array_size = size;
		if(array_size ==0) {
			return;
		}
		if(array_size <0) {
			try {
				array = new int[array_size];
				}
			catch(bad_alloc) {
				memoryException e;
					throw e;
				}
			}
	}
	~myArray() {
		if(array_size >0) {
			delete[] array;
		}
	};
	int at(int position) {
		if (position >= array_size) {
			try {
				array[position];
				position = array[position];
			}
			catch(out_of_range) {
				memoryException e;
				throw e;
			}
		}
		return position;
	};
	void resize(int new_size);
	void append(int element_to_add);

private:
	int *array;
	int array_size;
};


int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
	return 0;
}
