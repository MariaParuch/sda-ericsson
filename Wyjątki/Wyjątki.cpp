//============================================================================
// Name        : Wyj�tki.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Ansi-style
//============================================================================

#include <iostream>
using namespace std;

void last() {
	cout << "Last start" << endl;
	cout << "Throwing int exception" << endl;
	throw -1.0;

	cout << "Last end" << endl;

}

void third() {
	cout << "Third start" << endl;
	last();
	cout << "Third end" << endl;
}


void second() {
	cout << "Second start" << endl;
	try {
		third();
		}
	catch(double) {
		cout << "in second double exception" << endl;
		}
	cout << "Second end" << endl;
}

void first() {
	cout << "First start" << endl;
	try {
		second();
//	}
//	catch(int) {
//		cerr << "in first int exception" << endl;
	}
	catch(double) {
		cout << "in first double exception" << endl;
	}
	cout << "First end" << endl;

}

int main() {

	cout << "Main start" << endl;
	try {
		int x = 2;
		int y = 0;

		if(y==0)
			throw "error division by 0";
		int d = x/y;
	}
	catch(const char* exception) {
		cout << "In main any exception catched: " << exception << endl;

	}
	cout << "Main end" << endl;
	return 0;
}
