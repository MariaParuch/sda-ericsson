/*
 * Singleton.h
 *
 *  Created on: 10.05.2017
 *      Author: RENT
 */

#ifndef SINGLETON_H_
#define SINGLETON_H_
#include <iostream>

class Singleton {
public:
	virtual ~Singleton();
	static Singleton* getSingleton();
	int getYourCode(int yourCode);

private:
	Singleton();
	Singleton(Singleton const&);
	static Singleton* singleton;
	int yourCode;

};

#endif /* SINGLETON_H_ */
