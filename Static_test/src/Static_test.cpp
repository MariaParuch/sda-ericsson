//============================================================================
// Name        : Static_test.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "X.h"
#include "Singleton.h"
using namespace std;
int y = 8;

void f() {
	static int s = 0;
	int z = 0;
	cout << "Funkcja jest wywo�ana: " << s++ << " z: " << z++ << " zmienna globalna: " << y << endl;
}

int main() {
	static int s = 4;
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
	for(int i = 0; i<10; i++) {
		f();
		y +=2;
		s +=2;
	}

	X test_object1;
	X test_object2;
	X test_object3;
	X object(test_object3);

	object.printR();

	test_object2.setR(2);
	test_object2.getR();
	test_object2.printR();
	test_object3.printR();
	X::someStaticMethod();
	test_object1.someStaticMethod();
	Singleton *x = 0;
	x = Singleton::getSingleton();
	x->Singleton::getYourCode(14);


	return 0;
}
