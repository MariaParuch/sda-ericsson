/*
 * X.h
 *
 *  Created on: 10.05.2017
 *      Author: RENT
 */

#ifndef X_H_
#define X_H_
#include <iostream>

class X {
public:
	X();
	X(const X &copy);
	virtual ~X();
	int getR() const {return r;}
	void setR(int r) {this->r = r;}
	void printR() {std::cout << "r = " << r << std::endl;}
	static void someStaticMethod(){std::cout << "This is static method" << std::endl;}
private:
	static int r;
	static int counter;

};
#endif /* X_H_ */
