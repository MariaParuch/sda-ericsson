/*
 * Menu.cpp
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#include "Menu.h"
#include <iostream>
#include <string>


Menu::Menu() {
	rentCar = new RentCar;
}

Menu::~Menu() {
	// TODO Auto-generated destructor stub
}

void Menu::printMenu() {
	int option;
	while (true) {

		std::cout << "****************************************************"
				<< std::endl;
		std::cout << "********************\tMENU\t********************"
				<< std::endl;
		std::cout << "****************************************************"
				<< std::endl;
		std::cout << "1. Wy�wietl samochody" << std::endl;
		//	std::cout << "	1. Wszystkie samochody " << std::endl;
		//	std::cout << "	2. Dost�pne samochody " << std::endl;
		//	std::cout << "	3. Wypo�yczone samochody " << std::endl;
		std::cout
				<< "2. Dodaj nowy samoch�d (marka, model, rocznik, cena, tablica) "
				<< std::endl;
		std::cout << "3. Usu� samoch�d " << std::endl;
		std::cout << "4. Wynajmij" << std::endl;
		std::cout << "5. Zwroc" << std::endl;
		std::cout << "6. Modyfikuj" << std::endl;
		std::cout << "7. Wyjd�" << std::endl;


		std::cin >> option;
		switch (option) {
		case 1:
			rentCar->printAll();
			break;
		case 2:{
			std::string brand;
			std::string model;
			int year;
			int price;
			std::string ID;

			std::cout << "Podaj marke > ";
			std::cin >> brand;
			std::cout << "Podaj model > ";
			std::cin >> model;
			std::cout << "Podaj rok produkcji > ";
			std::cin >> year;
			std::cout << "Podaj cene za dzien wynajmu > ";
			std::cin >> price;
			std::cout << "Podaj numer tablicy rejestracyjnej > ";
			std::cin >> ID;

			rentCar->addNewPosition(brand, model, year, price, ID);
			break;
		}
		case 3:
		{
			rentCar->printAll();
			std::cout << "Podaj numer rejestracyjny > ";
			std::string ID;
			std::cin >> ID;
			rentCar->removePosition(ID);
			break;
		}
		case 4:
		{
			rentCar->printAvailable();
			std::cout << "Podaj numer rejestracyjny > ";
			std::string ID;
			std::cin >> ID;
			rentCar->rent(ID);
			break;
		}
		case 5:
			rentCar->returnCar();
			break;
		case 6:
			rentCar->printAll();
			rentCar->editPosition();
			break;
		case 7:
			return;

		default:
			std::cout << "Please correct your option" << std::endl;
			break;

		}
	}
}

