/*
 * Car.cpp
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#include "Car.h"
#include "sstream"

Car::Car() {
	bra = NoBrand;
	mod = NoModel;
	year = 0;
	price = 0;
	ID = "xxxxxxxx";
	available = true;
}

Car::Car(brand bra, model mod, int year, double price, std::string ID) {
	this->bra = bra;
	this->mod = mod;
	this->year = year;
	this->price = price;
	this->ID = ID;

	available = true;
}

Car::Car(std::string bra, std::string mod, int year, double price,
		std::string ID) {
	this->bra = stringToBrand(bra);
	this->mod = stringToModel(mod);
	this->year = year;
	this->price = price;
	this->ID = ID;

	available = true;
}
Car::~Car() {
	// TODO Auto-generated destructor stub
}

void Car::changeAvailable() {
	available = !available;
}

std::string Car::brandToString(brand bra) {
	switch (bra) {
	case Car::NoBrand:
		return "NoBrand";
		break;
	case Car::Mercedes:
		return "Mercedes";
		break;
	case Car::Audi:
		return "Audi";
		break;
	case Car::Porche:
		return "Porche";
		break;
	default:
		return "NoBrand";
	}
}

std::string Car::modelToString(model mod) {
	switch (mod) {
	case Car::NoModel:
		return "NoModel";
		break;
	case Car::SLS:
		return "SLS";
		break;
	case Car::TT:
		return "TT";
		break;
	case Car::Spider:
		return "Spider";
		break;
	default:
		return "NoModel";
	}
}

Car::brand Car::stringToBrand(std::string bra) {

	if (bra == "NoBrand") {
		return Car::NoBrand;
	} else if (bra == "Mercedes") {
		return Car::Mercedes;
	} else if (bra == "Audi") {
		return Car::Audi;
	} else if (bra == "Porche") {
		return Car::Porche;
	}
	return Car::NoBrand;

}

Car::model Car::stringToModel(std::string mod) {
	if (mod == "NoModel") {
		return Car::NoModel;
	} else if (mod == "SLS") {
		return Car::SLS;
	} else if (mod == "TT") {
		return Car::TT;
	} else if (mod == "Spider") {
		return Car::Spider;
	}
	return Car::NoModel;

}

std::string Car::describe() const {

	std::ostringstream ss;
	ss << brandToString(Car::bra) << " " <<  modelToString(mod) << " " << year << " " << ID << " " << price;
	std::string description = "";

	description = ss.str();

	return description;
}
