/*
 * RentCar.cpp
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#include "RentCar.h"

RentCar::RentCar() {
	// TODO Auto-generated constructor stub

}

RentCar::~RentCar() {
	// TODO Auto-generated destructor stub
}

void RentCar::printAll() {
	std::vector<Car>::iterator print = rental.begin();
	int id = 1;
	for (; print != rental.end(); ++print) {
		std::cout << id << ". " << print->describe() << std::endl;
		id++;
	}
}

void RentCar::printAvailable() {
	std::vector<Car>::iterator print = rental.begin();
	int id = 1;
	for (; print != rental.end(); ++print) {
		if (print->isAvailable()) {
			std::cout << id << ". " << print->describe() << std::endl;
			id++;
		}
	}
}

void RentCar::addNewPosition(std::string bra, std::string mod, int year,
		double price, std::string ID) {
	Car car(bra, mod, year, price, ID);
	rental.push_back(car);

}

void RentCar::removePosition(std::string ID) {
	std::vector<Car>::iterator searchId = rental.begin();
	for (unsigned int i = 0; i < rental.size(); ++i) {
		while (searchId != rental.end()) {
			if (rental[i].getId() == ID) {
				rental.erase(searchId);
				return;
			} else {
				searchId++;
			}
		}
	}
	std::cout << "Nr podanej rejestracji jest nieprawidlowy, sprobuj ponownie.";
}

void RentCar::rent(std::string ID) {
	std::vector<Car>::iterator rent;
	for (rent = rental.begin(); rent != rental.end(); rent++) {
		if (rent->getId() == ID) {
			rent->setAvailable(false);
			break;
		}
	}

//	std::cout << "Podaj date rozpoczecia wynajmu (dzien, miesiac, rok): "
//			<< std::endl;
//	int d, m, r;
//	std::cin >> d >> m >> r;
//	Date beginDate(r, m, d);
	std::cout << "Na ile dni chcesz wynajac samochod (podaj ilosc dni): "
			<< std::endl;
	int howManyDays;
	std::cin >> howManyDays;
	double rentPrice;
	rentPrice = (rent->getPrice()) * howManyDays;
	std::cout << "Cena za wynajem: " << rentPrice << std::endl;

}

void RentCar::returnCar() {
	std::cout << "Podaj numer rejestracyjny zwracanego samochodu: ";
	std::string ID;
	std::cin >> ID;

	for (std::vector<Car>::iterator it = rental.begin(); it != rental.end(); ++it){
		if (it->getId() == ID){
			it->changeAvailable();
			break;
		}
	}

}

void RentCar::editPosition() {
	std::cout << "Wybierz samochod (podaj nr samochodu): " << std::endl;
	int position;
	std::cin >> position;
	while (true) {
		if (position > 0 && position <= rental.size()) {
			std::vector<Car>::iterator insert = rental.begin();
			advance(insert, position);
			std::string bra;
			std::string mod;
			int year;
			double price;
			std::string ID;

			std::cout
					<< "Podaj nowe dane (marka, model, rocznik, cena, nr rejestracji) "
					<< std::endl;
			std::cout << "Marka: ";
			std::cin >> bra;
			std::cout << "Model: ";
			std::cin >> mod;
			std::cout << "Rocznik: ";
			std::cin >> year;
			std::cout << "Cena: ";
			std::cin >> price;
			std::cout << "Nr rejetracyjny: ";
			std::cin >> ID;

			Car car(bra, mod, year, price, ID);
			rental.insert(insert, car);
			insert = rental.begin();
			advance(insert, position - 1);
			rental.erase(insert);
			return;
		} else {
			std::cout << "Podany nr nie istnieje, podaj poprawn� wartosc."
					<< std::endl;
			std::cin >> position;
		}
	}
}

