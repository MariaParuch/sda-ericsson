/*
 * RentCar.h
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#ifndef SRC_RENTCAR_H_
#define SRC_RENTCAR_H_

#include <vector>
#include <string>
#include <iostream>
#include "Rental.h"
#include "Car.h"
#include "Date.h"


class RentCar {
public:
	RentCar();
	virtual ~RentCar();
	void printAll();
	void printAvailable();
	void addNewPosition(std::string bra, std::string mod, int year, double price, std::string ID);
	void removePosition(std::string ID);
	void rent(std::string ID);
	void returnCar();
	void editPosition();
	const std::vector<Car>& getRental() const {return rental;}
	int returnSize() {return rental.size();}

private:
	std::vector<Car> rental;
};

#endif /* SRC_RENTCAR_H_ */
