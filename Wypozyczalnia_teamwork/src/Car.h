/*
 * Car.h
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#ifndef CAR_H_
#define CAR_H_

#include <string>


class Car {
public:
	enum brand{NoBrand, Mercedes, Audi, Porche};
	enum model{NoModel, SLS, TT, Spider};

	Car();
	Car(brand bra, model mod, int year, double price, std::string ID);
	Car(std::string bra, std::string mod, int year, double price, std::string ID);

	virtual ~Car();

	void changeAvailable();
	static std::string brandToString(brand bra);
	static std::string modelToString(model mod);
	static Car::brand stringToBrand(std::string bra);
	static Car::model stringToModel(std::string mod);
	std::string describe() const;

	bool isAvailable() const {
		return available;
	}

	std::string getId() const {
		return ID;
	}

	const double& getPrice() const {
			return price;
		}

	void setAvailable(bool available) {
		this->available = available;
	}

	brand getBra() const {
		return bra;
	}

	void setBra(brand bra) {
		this->bra = bra;
	}

private:
	brand bra;
	model mod;
	std::string ID;
	int year;
	double price;
	bool available;
};

#endif /* CAR_H_ */
