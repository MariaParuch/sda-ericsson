/*
 * Rental.h
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#ifndef SRC_RENTAL_H_
#define SRC_RENTAL_H_
#include <iostream>

class Rental {
public:
	Rental();
	virtual ~Rental();
	virtual void printAll() = 0;
	virtual void addNewPosition() = 0;
	virtual void removePosition() = 0;
	virtual void rent() = 0;
	virtual void editPosition() = 0;
};

#endif /* SRC_RENTAL_H_ */
