/*
 * Menu.h
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#ifndef SRC_MENU_H_
#define SRC_MENU_H_

#include "RentCar.h"

class Menu {
public:
	Menu();
	virtual ~Menu();
	void printMenu();

private:
	RentCar *rentCar;
};

#endif /* SRC_MENU_H_ */
