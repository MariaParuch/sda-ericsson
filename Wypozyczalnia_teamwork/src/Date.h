/*
 * Date.h
 *
 *  Created on: 05.06.2017
 *      Author: RENT
 */

#ifndef DATE_H_
#define DATE_H_

class Date {
public:
	Date();
	Date(int y, int m, int d);
	virtual ~Date();
	int getDay() const {return day;}
	void setDay(int day) {this->day = day;}
	int getMonth() const {return month;}
	void setMonth(int month) {this->month = month;}
	int getYear() const {return year;}
	void setYear(int year) {this->year = year;}

private:
	int year;
	int month;
	int day;
};

#endif /* DATE_H_ */
