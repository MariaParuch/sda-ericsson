/*
 * CarTests.cpp
 *
 *  Created on: 03.06.2017
 *      Author: RENT
 */
#include "gtest.h"
#include "../src/Car.h"
#include <iostream>
#include <string>
//brand to string

TEST(brandToStringTest, Audi) {
	Car test1;
	EXPECT_EQ("Audi", test1.brandToString(Car::Audi));
}
TEST(brandToStringTest, Merc) {
	Car test1;
	EXPECT_EQ("Mercedes", test1.brandToString(Car::Mercedes));
}
TEST(brandToStringTest, Porche) {
	Car test1;
	EXPECT_EQ("Porche", test1.brandToString(Car::Porche));
}
TEST(brandToStringTest, NoBrand) {
	Car test1;
	EXPECT_EQ("NoBrand", test1.brandToString(Car::NoBrand));
}

// model to string
TEST(modelToStringTest, SLS) {
	Car test1;
	EXPECT_EQ("SLS", test1.modelToString(Car::SLS));
}
TEST(modelToStringTest, TT) {
	Car test1;
	EXPECT_EQ("TT", test1.modelToString(Car::TT));
}
TEST(modelToStringTest, Spider) {
	Car test1;
	EXPECT_EQ("Spider", test1.modelToString(Car::Spider));
}
TEST(modelToStringTest, NoModel) {
	Car test1;
	EXPECT_EQ("NoModel", test1.modelToString(Car::NoModel));
}
// string to brand
TEST(stringToBrandTest, NoBrand) {
	Car test1;
	EXPECT_EQ(Car::NoBrand, test1.stringToBrand("NoBrand"));
}
TEST(stringToBrandTest, Mercedes) {
	Car test1;
	EXPECT_EQ(Car::Mercedes, test1.stringToBrand("Mercedes"));
}
TEST(stringToBrandTest, Audi) {
	Car test1;
	EXPECT_EQ(Car::Audi, test1.stringToBrand("Audi"));
}
TEST(stringToBrandTest, Porche) {
	Car test1;
	EXPECT_EQ(Car::Porche, test1.stringToBrand("Porche"));
}
TEST(stringToBrandTest, Else) {
	Car test1;
	EXPECT_EQ(Car::NoBrand, test1.stringToBrand("something"));
}
TEST(stringToBrandTest, Number) {
	Car test1;
	EXPECT_EQ(Car::NoBrand, test1.stringToBrand("15"));
}
// string to model
TEST(stringToModelTest, NoModel) {
	Car test1;
	EXPECT_EQ(Car::NoModel, test1.stringToModel("NoModel"));
}
TEST(stringToModelTest, SLS) {
	Car test1;
	EXPECT_EQ(Car::SLS, test1.stringToModel("SLS"));
}
TEST(stringToModelTest, Spider) {
	Car test1;
	EXPECT_EQ(Car::Spider, test1.stringToModel("Spider"));
}
TEST(stringToModelTest, TT) {
	Car test1;
	EXPECT_EQ(Car::TT, test1.stringToModel("TT"));
}
TEST(stringToModelTest, Number) {
	Car test1;
	EXPECT_EQ(Car::NoModel, test1.stringToModel("1231231"));
}
TEST(stringToModelTest, Else) {
	Car test1;
	EXPECT_EQ(Car::NoModel, test1.stringToModel("Something different"));
}

