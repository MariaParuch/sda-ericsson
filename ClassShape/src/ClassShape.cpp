//============================================================================
// Name        : ClassShape.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
#include "Shape.h"
#include "Triangle.h"
#include "Quadrangle.h"
#include "Rectangle.h"
#include "Square.h"
#include "Circle.h"
#include "Pentagon.h"


int main() {

Shape *shapes[5];

Triangle trojkat ("trojkat", 3.4, 2.7);
Circle kolo ("k�ko", 3.4);
Rectangle prostokat ("prostok�t", 3.4, 2.7);
Square kwadrat ("kwadrat", 3.4,3.4);
Pentagon pieciokat ("pi�ciok�t", 2.7);

shapes[0] = &trojkat;
shapes[1] = &kwadrat;
shapes[2] = &prostokat;
shapes[3] = &pieciokat;
shapes[4] = &kolo;

for(int i = 0; i<5; ++i){
	shapes[i] -> getName();
	shapes[i] -> getArea();
	shapes[i] -> describe();
}
	return 0;
}
