/*
 * Quadrangle.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef QUADRANGLE_H_
#define QUADRANGLE_H_

#include "Shape.h"
#include <string>

class Quadrangle: public Shape {
public:
Quadrangle(double s, double h);
double getArea();

protected:
double side;
double height;

};

#endif /* QUADRANGLE_H_ */
