/*
 * Pentagon.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef PENTAGON_H_
#define PENTAGON_H_

#include "Shape.h"
#include <string>
#include <iostream>

class Pentagon: public Shape {
public:
Pentagon(std::string n, double s);
void describe();
double getArea();

private:
double side;
};

#endif /* PENTAGON_H_ */
