/*
 * Square.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef SQUARE_H_
#define SQUARE_H_

#include "Quadrangle.h"
#include <string>
#include <iostream>

class Square: public Quadrangle{
public:
	Square(std::string n, double s, double h);
	void describe();
//	void drawShape();
};

#endif /* SQUARE_H_ */
