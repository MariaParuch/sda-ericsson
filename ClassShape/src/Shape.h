/*
 * Shape.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef SHAPE_H_
#define SHAPE_H_
#include <string>
#include <iostream>

class Shape {
public:
	std::string getName();
	virtual void describe() = 0;
	virtual double getArea() = 0;
//	virtual void drawShape() = 0;
protected:
	std::string name;
	double area;
};

#endif /* SHAPE_H_ */
