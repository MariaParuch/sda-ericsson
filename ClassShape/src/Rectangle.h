/*
 * Rectangle.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef RECTANGLE_H_
#define RECTANGLE_H_
#include <string>
#include <iostream>

#include "Quadrangle.h"

class Rectangle: public Quadrangle {
public:
	Rectangle(std::string n, double s, double h);
	void describe();

};

#endif /* RECTANGLE_H_ */
