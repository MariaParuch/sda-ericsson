/*
 * Circle.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef CIRCLE_H_
#define CIRCLE_H_

#include "Shape.h"
#include <string>
#include <iostream>

class Circle: public Shape {
public:
Circle(std::string n, double r);
void describe();
double getArea();

private:
double radius;
};

#endif /* CIRCLE_H_ */
