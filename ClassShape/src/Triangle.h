/*
 * Triangle.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef TRIANGLE_H_
#define TRIANGLE_H_

#include "Shape.h"
#include <string>

class Triangle: public Shape {
public:
Triangle(std::string n, double s, double h);
void describe();
double getArea();
//void drawShape();

private:
double side;
double height;
};

#endif /* TRIANGLE_H_ */
