//============================================================================
// Name        : Data.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "ClassData.h"
using namespace std;

int main()
{
    Data dzis(2017,03,23);
    Data tommorow(2017,03,24);

//    std:: cout << (dzis == tommorow) << endl;
    std:: cout << dzis.operator == (tommorow) << endl;
    std:: cout << dzis.operator!=(tommorow) << endl;
    std:: cout << dzis.operator > (tommorow) << endl;
    std:: cout << tommorow.operator > (dzis) << endl;
    std:: cout << dzis.daysToTheEndOfTheYear() << endl;


//    cout<< dzis.getYear() <<" ";
//    cout<< dzis.getMonth() <<" ";
//    cout<< dzis.getDay() <<" ";
    dzis.printData();
    dzis.czyPrzestepny();
    dzis.isValid();

    return 0;
}
