/*
 * ClassData.cpp
 *
 *  Created on: 22.03.2017
 *      Author: RENT
 */

#include "ClassData.h"
#include <iostream>

Data::Data(int y, int m, int d)
{
    year = y;
    month = m;
    day = d;
}

int Data::getYear()
{
    return year;
}
int Data::getMonth()
{
    return month;
}
int Data::getDay()
{
    return day;
}
bool Data::operator==(const Data& other)const
{
	if (day == other.day && month == other.month && year == other.year)
		return true;
	return false;
}
bool Data::operator >(const Data& other)const
{
	if(year>other.year)
		return true;
	if(year == other.year && month>other.month)
		return true;
	if(year == other.year && month == other.month && day>other.day)
		return true;
	return false;
}
bool Data::operator!=(const Data& other)const
{
	return !operator ==(other);
	return !(*this == other);
}

bool Data::czyPrzestepny()const
{
	if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
	        return true;
	       } else {
	        return false;
	       }
}
bool Data::isValid()
{
	 switch(month) {
	    case 1:
	    case 3:
	    case 5:
	    case 7:
	    case 8:
	    case 10:
	    case 12: {
	        if (day<=31 && day>=1){
	        	std::cout << "Data poprawna";
	           return true;
	        } else {
	        	std::cout << "Data niepoprawna";
	            return false;
	        }
	    }
	    case 4:
	    case 6:
	    case 9:
	    case 11: {
	        if (day<=30 && day>=1) {
	        	std::cout << "Data poprawna";
	           return true;
	        } else {
	        	std::cout << "Data niepoprawna";
	           return false;
	        }
	    }
	    case 2: {
	        if (czyPrzestepny()==true && day <=29 && day>=1) {
	        	std::cout << "Data poprawna";
	           return true;
	        } else if (czyPrzestepny()==false && day <=28 && day>=1) {
	        	std::cout << "Data poprawna";
	           return true;
	        } else {
	        	std::cout << "Data niepoprawna";
	           return false;
	        }
	    }
}
	 return 0;
}
void Data::printData()
{
	std::cout << day << " " << month <<" " << year << std::endl;
}

int Data::daysToTheEndOfTheYear()const
{
int leapYear[12] = {31,29,31,30,31,30,31,31,30,31,30,31};
int classicYear[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
int daysToTheEnd = 0;
	if (czyPrzestepny() == true){
	for(int i = month; i<12; ++i){
	daysToTheEnd +=leapYear[i];
	}
	daysToTheEnd += leapYear[month-1] - day;
	}else {
	for(int i = month; i<12; ++i){
	daysToTheEnd +=classicYear[i];
	}
	daysToTheEnd += classicYear[month-1] - day;
	}
	return daysToTheEnd;
}
//int Data::addDays(int daysToAdd)
//{
//			    case 1:
//			    case 3:
//			    case 5:
//			    case 7:
//			    case 8:
//			    case 10:{
//			        if (day>31){
//			        	month++;
//			        	day -=31;
//			        	std::cout << "Dzie� " << day << "miesiac " << month << "rok" << year << std::endl;
//
//			        } else {
//			        	std:: cout << "Dzie� " << day << "miesiac " << month << "rok" << year << std::endl;
//			        }
//			    case 4:
//			    case 6:
//			    case 9:
//			    case 11: {
//			    	 if (day>30) {
//			    		month++;
//			    		day -=30;
//			    		std::cout << "Dzie� " << day << "miesiac " << month << "rok" << year << std::endl;
//			    	    } else {
//			    	    std:: cout << "Dzie� " << day << "miesiac " << month << "rok" << year << std::endl;
//			    	        }
//			    	    }
//			    	    case 2: {
//			    	        if (czyPrzestepny()==true && day <=29 && day>=1) {
//			    	        	std::cout << "Data poprawna";
//			    	           return true;
//			    	        } else if (czyPrzestepny()==false && day <=28 && day>=1) {
//			    	        	std::cout << "Data poprawna";
//			    	           return true;
//			    	        } else {
//			    	        	std::cout << "Data niepoprawna";
//			    	           return false;
//			    	        }
//			    	    }
//			    }
//			    	 return 0;
//
//}
//int Data:: addMonth()
//{
//	int months;
//
//}


