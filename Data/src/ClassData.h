/*
 * ClassData.h
 *
 *  Created on: 22.03.2017
 *      Author: RENT
 */

#ifndef DATA_H
#define DATA_H

class Data
{
public:
    Data(int y, int m, int d);
    int getYear();
    int getMonth();
    int getDay();
//    void setYear(int r);
//    void setMonth(int a);
//    void setDay(int b);
    bool isValid();
    bool czyPrzestepny()const;
    void printData();
    bool operator==(const Data& other)const;
    bool operator!=(const Data& other)const;
    bool operator >(const Data& other)const;
    int daysToTheEndOfTheYear()const;
//    int addDays(int days);
//    int addMonth(int months);

private:
    int year;
    int month;
    int day;
};

#endif // DATA_H
