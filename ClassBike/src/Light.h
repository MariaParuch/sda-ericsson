/*
 * Light.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef LIGHT_H_
#define LIGHT_H_

class Light{
public:

	Light();
	void turnOnTheLight(){lightIsOn = true;};
	void turnOffTheLight(){lightIsOn = false;};
	bool isTurnOnTheLight(){return lightIsOn;};



private:
	bool lightIsOn;

	};
#endif /* LIGHT_H_ */
