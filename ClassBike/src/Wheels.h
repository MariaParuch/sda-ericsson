/*
 * Wheels.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef WHEELS_H_
#define WHEELS_H_

class Wheels {

private:
    int frontWheelSize;
    int rearWheelSize;
public:
    Wheels();
    Wheels(int size);

    int getFrontWheelSize() const {return frontWheelSize;};
    int getRearWheelSize() const {return rearWheelSize;};

    void setFrontWheelSize(int size) {frontWheelSize = size;};
    void setRearWheelSize(int size) {rearWheelSize = size;};

};


#endif /* WHEELS_H_ */
