/*
 * Bike.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef BIKE_H_
#define BIKE_H_
#include "Wheels.h"
#include "Frame.h"
#include "DriveSystem.h"
#include "Light.h"

class Bike {
public:
	Bike();
	void turnOnTheLight();
	void turnOffTheLight();

private:
	Frame frame;
	Frame::frameType::hardtail;
//	Wheels wheel;
	Light frontLight;
	Light rearLight;
//	DriveSystem chain;
//	DriveSystem pedal;
//	DriveSystem rearDerailleur;
//	DriveSystem frontDerailleur;


};

#endif /* BIKE_H_ */


//rower posiada:
//	ko�a x2 kl
//		typ opony
//		srednica ko�a
//	rama kl
//	sztywna, amortyzowana
//	hamulec x2
//	amortyzator
//		rodzaj
//	kierownica
//	siode�ko
//	oswietlenie(prz�d, ty�, odblaski) kl
//	 metody:
//	 - w��cz
//	 - wy��cz
//	uk�ad nap�dowy: kl
//		�a�cuch
//		biegi prz�d
//		biegi ty�
//		przerzutki
//		peda�a
//	metody:
//	- zmie� bieg
//
//metody:
//jed�
//hamuj
//w��cz �wiat�a
//skr�c (zmie� kierunek)
//zmie� bieg - jedna metoda z 2 arg (przerzutka prz�d|ty�, kierunek g�ra|d�, albo 1 bez arg)
