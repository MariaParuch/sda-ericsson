/*
 * Wheels.cpp
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#include "Wheels.h"

Wheels::Wheels() {
	frontWheelSize = 18;
	rearWheelSize = 18;

}

Wheels::Wheels(int size) {
	frontWheelSize = size;
	rearWheelSize = size;
}
