//============================================================================
// Name        : ExampleFunctionWithArray.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int solution(int *A, int size_of_A);

int main() {
	int myArray[10] = {-2,5,8,10,-10,178,-111111,0, 10, 100000};
	solution(myArray, 10);

	//test
	int A[] = {-2147483648, 0, 2147483647};
	int B[] = {-10, 0, 20000, 100, -1, 9};
	int C[] = {-10, 0, 20000, 100, -1, -90};
	int D[] = {15, 11, 0, -14, -14, 10};
	solution(A, 3);
	solution(B, 6);
	solution(C, 6);
	solution(D, 6);

	return 0;
}
int solution(int *A, int size_of_A) {
	int min = A[0];
	for(int i = 0; i<size_of_A; i++) {
		if (A[i] < min){
			min = A[i];
		}
	}
	cout << "min = " << min << endl;
	return min;
}
