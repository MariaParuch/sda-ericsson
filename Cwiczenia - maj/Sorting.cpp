//============================================================================
// Name        : Sorting.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <ctime>
#include <iomanip>
using namespace std;

void insert_sort(int *array, int n);
void merge(int *array, int p, int q, int r);
void merge_sort(int *array, int p, int r);
void quick_sort(int *array, int p, int r);
int partition (int *array, int p, int r);

int main() {

	int myArray[10] = {23,11,2,0,321,-12,484,9,46,-1};
	clock_t tStart = clock();
	quick_sort(myArray, 0, 9);
	cout << "Time taken for qsort: " << fixed << setprecision(3)
		 << (static_cast<double>(clock()) - tStart) /CLOCKS_PER_SEC << "\n";
	cout << "qsort: " << endl;
	for(int i = 0; i<10; i++) {
		cout << myArray[i] << endl;
	}
	int myArray1[10] = {23,11,2,0,321,-12,484,9,46,-1};
	clock_t qStart = clock();
	insert_sort(myArray1, 10);
	cout << "Time taken for qsort: " << fixed << setprecision(3)
		 << (static_cast<double>(clock()) - qStart) /CLOCKS_PER_SEC << "\n";
	cout << "isort: " << endl;
	for(int i = 0; i<10; i++) {
		cout << myArray[i] << endl;
	}

	return 0;
}

void insert_sort(int *array, int n) {
	int key, i, j;
	for(i = 2; i<n; i++){
		key = array[i];
		j = i-1;
		while(j >= 0 && array[j] > key){
			array[j + 1]= array[j];
			j = j-1;
			}
		array[j + 1]= key;
	}
	for(int m = 0; m<n; m++) {
		cout << array[m] << endl;
	}
}

void merge(int array[], int p, int q, int r) {
	int n1 = q-p + 1;
	int n2 = r-q;
	int i,j,k;
	int L[i];
	int R[j];
	int inf;
// let L[1.. n1 + 1]and R[1 .. n2 + 1]be new arrays
	for(i = 1; i < n1; i++) {
		L[i]= array[p + i-1];
	}
	for(j = 1; j < n2; j++) {
		R[j] = array[q + j];
	}
	L[n1 + 1]= inf;
	R[n2 + 1]= inf;
	i = 1;
	j = 1;
	for(k = p; k < r; k++){
		 if(L[i]<=R[j]){
			array[k]= L[i];
			i = i + 1;
		 } else {
			array[k]= R[j];
			j = j + 1;
		 }
	}
//	for(int m = 0; m<10; m++) {
//			cout << array[m] << endl;
//	}
}
void merge_sort(int *array, int p, int r) {
	int q;
	if (p < r) {
	 q = ((p + r)/2);
	 merge_sort(array, p, q);
	 merge_sort(array, q + 1, r);
	 merge(array, p, q, r);
	}
}
void quick_sort(int *array, int p, int r) {
	if(p < r) {
	 int q = partition(array, p, r);
	 quick_sort(array, p, q - 1);
	 quick_sort(array, q + 1, r);
	}
}


int partition (int *array, int p, int r) {
	int x = array[r];
	int i = p - 1;

	for(int j = p; j <= (r - 1); j++) {
		if(array[j] <= x) {
			i = i + 1;
			swap(array[i], array[j]);
		}
	}
	swap(array[i + 1], array[r]);
	return i + 1;
}

